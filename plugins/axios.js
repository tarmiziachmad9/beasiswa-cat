export default function ({ $axios, redirect, store, req }) {
  $axios.onRequest(config => {
    // Client
    const token = JSON.parse(localStorage.getItem('dataToken'))
    if (token) {
      config.headers.common['Authorization'] = 'Bearer ' + token
      return config
    }
    return config
  })

  $axios.onResponse(response => {
    if(response.message) {
      if(response.message === 'Token Missing' || response.message === 'Token Expired' || response.code === 401) {
        localStorage.removeItem('dataToken')
        return Promise.reject(response, 
          redirect('/login'))
      }
    }
    return response
  })

  $axios.onError(error => {
    console.log(error.response.data.message)
    if(error.response) {
      if(error.response.data.message === 'Token Missing' || error.response.data.message === 'Token Expired') {
        localStorage.removeItem('dataToken')
        return Promise.reject(error, 
          redirect('/login'))
      }
    }
    return Promise.reject(error)
  })
}
