export default function ({ store, error, redirect}) {
  const data = JSON.parse(localStorage.getItem('stateStart'))
  if(data) {
    redirect('/')
  }
}