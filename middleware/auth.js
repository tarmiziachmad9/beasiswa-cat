export default function ({ store, error, redirect}) {
  const token = JSON.parse(localStorage.getItem('dataToken'))
  const data = JSON.parse(localStorage.getItem('stateStart'))
  const stateFinish = JSON.parse(localStorage.getItem('stateFinish'))
  if(!data) {
    redirect('/login')
  }
}