export default function ({ store, error, redirect}) {
  const token = JSON.parse(localStorage.getItem('dataToken'))
  if(!token) {
    redirect('/login')
  }
}