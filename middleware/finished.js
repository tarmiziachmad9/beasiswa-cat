export default function ({ store, error, redirect}) {
  const state = JSON.parse(localStorage.getItem('stateFinished'))
  if(state) {
    redirect('/finished')
  }
}